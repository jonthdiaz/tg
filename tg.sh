#!/bin/sh
now=$(date)
to=$1
subject=$(echo $@ | cut -d " " -f 2-)
tgpath=/home/tutorya/webapps/tutorya/tutorya/tg
LOGFILE="/home/tutorya/webapps/tutorya/tutorya/logs/tg.log"
cd ${tgpath}
${tgpath}/bin/telegram-cli -k ${tgpath}/tg-server.pub -W <<EOF
msg $to $subject
safe_quit
EOF
echo "$now Recipient=$to Message="$subject"" >> ${LOGFILE}
echo "Finished" >> ${LOGFILE}